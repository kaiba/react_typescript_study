import { ActionTypes } from "../actions/ActionTypes";
import { AddIncomeAction } from "../actions/AddIncomeAction";
import { IncomeState } from "../states/IncomeState";

const incomeList = (
  state: IncomeState[] = [],
  action: AddIncomeAction
): IncomeState[] => {
  switch (action.type) {
    case ActionTypes.ADD_INCOME:
      return [
        ...state,
        {
          id: action.payload.id,
          income: action.payload.income
        }
      ];
    default:
      return state;
  }
};

export default incomeList;
