import * as React from "react";
import "./App.css";

export interface State {}
export interface Props {
  income: number;
}

class Income extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    // メソッド内でthisを見るためのお約束
    this.moneyYouWant = this.moneyYouWant.bind(this);
  }

  moneyYouWant(money: number): string {
    if (money === NaN || money <= 0) {
      return "";
    }
    return `${money * 10000000}兆円欲しい`;
  }

  render() {
    return <p className="Money">{this.moneyYouWant(this.props.income)}</p>;
  }
}

export default Income;
