import * as React from "react";
import { Dispatch, Action } from "redux";
import { connect } from "react-redux";
import "./App.css";
import Income from "./Income";
import { addIncome } from "./actions/AddIncomeAction";

export interface Props {
  dispatch: Dispatch<Action>;
}
export interface State {
  inputingIncome: string;
  incomes: Array<number>;
}

const logo = require("./logo.svg");

class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    // メソッド内でthisを見るためのお約束
    this.addIncome = this.addIncome.bind(this);
    this.onChangeIncome = this.onChangeIncome.bind(this);
  }

  onChangeIncome(e: React.FormEvent<HTMLInputElement>) {
    this.setState({
      inputingIncome: e.currentTarget.value
    });
  }

  addIncome(e: React.FormEvent<HTMLButtonElement>) {
    //let arr = this.state.incomes;
    let income = Number(this.state.inputingIncome);
    if (isNaN(income)) {
      return;
    }
    this.props.dispatch(addIncome(income));
  }

  render() {
    // keyを渡さないと警告がでた。index番号をkeyとした。
    let incomes = this.state.incomes.map((i, index) => {
      return <Income key={index} income={i} />;
    });
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          <label>
            現在の年収
            <input
              type="text"
              value={this.state.inputingIncome}
              onChange={this.onChangeIncome}
            />万円
          </label>
          <button onClick={this.addIncome}>年収追加</button>
        </p>
        {incomes}
      </div>
    );
  }
}

export default connect()(App);
