import { Action } from "redux";
import { ActionTypes } from "./ActionTypes";

export interface AddIncomeAction extends Action {
  type: ActionTypes.ADD_INCOME;
  payload: { id: number; income: number };
}

let incomeId: number = 0;

export const addIncome = (income: number): AddIncomeAction => ({
  type: ActionTypes.ADD_INCOME,
  payload: {
    id: (incomeId += 1),
    income
  }
});
