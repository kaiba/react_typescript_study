export interface IncomeState {
  readonly id: number;
  readonly income: number;
}
